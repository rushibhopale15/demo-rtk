import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "counter",
  initialState: {
    count: 0,
  },
  reducers: {
    increseCount(state, action) {
      state.count++;
      console.log(state);
    },
    decreseCount(state, action) {
      state.count--;
    },
    setToZero(state, action) {
        state.count=0
    },
  },
});

console.log(userSlice.actions);
export default userSlice;
export const { increseCount, decreseCount, setToZero } = userSlice.actions;
