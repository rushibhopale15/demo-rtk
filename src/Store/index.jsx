import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./Slice/Slice";

const store = configureStore({
  reducer: { counter1: userSlice.reducer },
});

export default store;
