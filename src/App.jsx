import { useState } from "react";
import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { decreseCount, increseCount, setToZero } from "./Store/Slice/Slice";

function App() {
  const dispatch = useDispatch();
  
  const count = useSelector((state) => state.counter1.count);

  return (
    <div style={{ width: "500px", margin: "auto", textAlign: "center" }}>
      <h2>Counter App</h2>
      <p>{count}</p>
      <button style={{padding:"10px", margin:'10px'}} onClick={() => dispatch(increseCount())}>+</button>
      <button style={{padding:"10px", margin:'10px'}} onClick={() => dispatch(decreseCount())}>-</button>
      <button style={{padding:"10px", margin:'10px'}} onClick={() => dispatch(setToZero())}>zero</button>
    </div>
  );
}

export default App;
